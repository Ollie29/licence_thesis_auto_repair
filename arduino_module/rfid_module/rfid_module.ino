#include <SPI.h>
#include <MFRC522.h>
#include "RTClib.h"
#include <Wire.h>

#define SS_PIN 53
#define RST_PIN 5

MFRC522 rfid(SS_PIN, RST_PIN);
RTC_DS3231 rtc;

String ssid = "DIGI-24-746AD1";
String password = "12345678r"; 
//String cipStartString = "\"TCP\",\"192.168.1.107\",9100\r\n";
String cipStartString = "\"TCP\",\"infinite-shelf-64040.herokuapp.com\",80\r\n";
String responseCard = "";

String constructAPI(String data, DateTime now)
{
  int contentLength = 27 + 25 + String(now.year(), DEC).length() + 
      String(now.month(), DEC).length() + String(now.day()).length() + 
      String(now.hour(), DEC).length() + String(now.minute(), DEC).length() + 
      String(now.second(), DEC).length(); //27 for the tag id, remains unchanged
  //Serial.println("THIS IS SPARTA: " + contentLength);
  String myAPI = "POST /api/user/rfid/attend HTTP/1.1\r\n";
  myAPI += "Host: infinite-shelf-64040.herokuapp.com\r\n";
  //myAPI += "Host: 192.168.1.107:9100\r\n";
  myAPI += "Content-Type: application/json\r\n";
  myAPI += "Content-Length: ";
  myAPI += contentLength;
  myAPI += "\r\n\r\n";
  myAPI += "{ \r\n  \"uid\" : ";
  myAPI += "\"";
  myAPI += data;
  myAPI += "\",\r\n"; 
  myAPI += "  \"timedate\" : ";
  myAPI += "\"";
  myAPI += now.year();
  myAPI += "/";
  myAPI += now.month();
  myAPI += "/";
  myAPI += now.day();
  myAPI += " ";
  myAPI += now.hour();
  myAPI += ":";
  myAPI += now.minute();
  myAPI += ":";
  myAPI += now.second();
  myAPI += "\"\r\n";
  myAPI += "}\r\n";
  return myAPI;
}

void setup() {
  Serial.begin(115200); //init serial communication
  Serial1.begin(115200);
  delay(2000);

 if (!rtc.begin()) {
    Serial.println("RTC not found");
  }
  if (rtc.lostPower()) {
    Serial.println("RTC lost power, readjusting time...");
  
  // rtc.adjust(DateTime(2020, 8, 216, 12, 34, 0));
  }
  
  SPI.begin(); //init SPI bus
  rfid.PCD_Init(); //init MFRC522
  Serial.println("Ready to read...");
  Serial.println();
  Serial1.println("AT+RST\r\n");
  ownDelay(3000, millis());
  Serial1.println("AT+CWMODE=1\r\n");
  ownDelay(2000, millis());
  Serial1.flush();
  connectToWAP();
  connectToHostServer();
}

void ownDelay(int interval, unsigned long sMillis)
{
  while(millis() - sMillis < interval) {}
}

void connectToWAP()
{
  Serial1.println("AT+CWJAP=\"" + ssid + "\"" + "," +  "\"" + password + "\"" + '\r' + '\n');
  Serial.println("SERIAL: AT+CWJAP=\"" + ssid + "\"" + "," +  "\"" + password + "\"" + '\r' + '\n');
  ownDelay(3000, millis());
}

void connectToHostServer()
{
  Serial1.flush();
  ownDelay(5500, millis()); //always sent before RST, and everything resetted
  Serial1.println("AT+CIPSTART=" + cipStartString);
  Serial.println("AT+CIPSTART=" + cipStartString);
  if(Serial1.find("ERROR"))
  {
    Serial.println("INFO: AT+CIPSTART ERROR");
  }
}

void sendData(String jsonDATA)
{
  DateTime now = rtc.now();
  String myAPI = constructAPI(jsonDATA, now);
  Serial1.println("AT+CIPSEND=" + String(myAPI.length())); //CIPSEND supports max 2048 B
  if(Serial1.find((char *)">"))
  {
    Serial1.println(myAPI);
    ownDelay(1000, millis());
  }
  Serial.println(myAPI);
}

String readCard()
{
  String response = "";
  if(rfid.PICC_ReadCardSerial()) //checks if read successfully
  {
    for(byte i = 0; i < rfid.uid.size; i++)
    {
      Serial.println(rfid.uid.uidByte[i], HEX);
      response += String(rfid.uid.uidByte[i], HEX);
    }
    ownDelay(2000, millis());
  }
 // Serial.println("Response from readCard() => " + response);
  return response;
}

void loop() {

  if((rfid.PICC_IsNewCardPresent())) //looks for new cards
  {
    //connectToHostServer();
    responseCard = readCard();
  }

  if(!responseCard.equals(""))
  {
      Serial.println("Preparing to send: " + responseCard);
      //sendData("{ \"uid\": \"" + responseCard + "\" }");
      sendData(responseCard);
      responseCard = "";
  }

  while(Serial1.available())
  {
    char c = Serial1.read();
    Serial.write(c);
  }

   while(Serial.available())
   {
    Serial1.write(Serial.read());
    //Serial.println(test);
   }
   
}

<?php

use App\Employee;
use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

Broadcast::channel('time.{uid}', function ($user, $uid) {
    return $user->employee->company->id === (int)$uid; //int because laravel makes the $uid a string (" ")
        //Employee::where('rfid_uid', $uid)->first()->company; //can connect if
    // the company of the user that scans the tag is the same as the logged in user's company
});

Broadcast::channel('partsStock.{uid}', function ($user, $uid) {
    return true;
});

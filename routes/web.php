<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Route::group(['prefix' => '/workflow', 'middleware' => 'auth'], function() {
    Route::get('', 'WorkflowController@index')->name('workflow.index');
});

Route::group(['prefix' => '/orders', 'middleware' => 'auth'], function() {
    Route::get('', 'OrderController@index')->name('orders.index');
//    Route::get('/{id}', 'OrderController@edit')->name('orders.edit');
});

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/plans', 'SubscriptionController@index')->name('subscription.index')->middleware('auth');

Route::group(['prefix' => '/company', 'middleware' => 'auth'], function() {
    Route::get('', 'CompanyController@index')->name('company.index');
    Route::get('/employees', 'EmployeeController@index')->name('employee.index');
    Route::put('/update', 'CompanyController@store')->name('company.store');
});

Route::group(['prefix' => '/timeclocks', 'middleware' => 'auth'], function() {
    Route::get('', 'TimeClockController@index')->name('timeclocks.index');
});

Route::group(['prefix' => '/inventory', 'middleware' => 'auth'], function() {
    Route::get('/parts', 'PartController@index')->name('parts.index');
});

Route::group(['prefix' => '/calendar', 'middleware' => 'auth'], function() {
    Route::get('', 'ScheduleController@index')->name('schedule.index');
});


Route::group(['prefix' => '/reports', 'middleware' => 'auth'], function() {
    Route::get('/customers', 'ReportController@index')->name('report.index');
});

Route::group(['prefix' => '/admin'], function() {
    Route::get('/panel', 'SuperAdminController@index')->name('admin.index');
});


Auth::routes();

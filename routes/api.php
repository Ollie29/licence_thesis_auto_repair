<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'estimate', 'middleware' => ['auth:sanctum']], function() {
    Route::post('/store', 'OrderController@storeEstimate');
    Route::delete('/delete/{id}', 'OrderController@deleteEstimate');
    Route::get('/get/all', 'OrderController@getAll');
    Route::post('/export', 'OrderController@exportEstimate');
    Route::get('/print/{id}', 'OrderController@getInvoice');
    Route::get('/invoices', 'OrderController@getAllInvoices');
    Route::post('/pay/{id}', 'OrderController@payEstimate');
    Route::get('/get/unpaid', 'OrderController@getUnpaidInvoices');
});

Route::group(['prefix' => 'services', 'middleware' => ['auth:sanctum']], function() {
    Route::get('/get/all', 'ServiceController@getAllCompanyServices');

});

Route::group(['prefix' => 'users', 'middleware' => ['auth:sanctum']], function() {
    Route::get('/get/all', 'UserController@getAllWrapper');
    Route::get('/get/last/{days}', 'UserController@getUsersUnderDate');
    Route::get('/get/active', 'UserController@getUsersStatistics');
    Route::get('/get/types', 'UserController@getUserTypeStatistics');
    Route::put('/update/{id}', 'UserController@update');
    Route::delete('/delete/{id}', 'UserController@delete');
});

Route::group(['prefix' => 'clock', 'middleware' => ['auth:sanctum']], function() {
 //  Route::post('/insert', 'TimeClockController@store');
   Route::get('/all', 'TimeClockController@getClocks');

});

Route::group(['prefix' => 'user', 'middleware' => 'auth:sanctum'], function() {
    Route::put('/update', 'EmployeeController@updateEmployee');
    Route::post('/insert', 'EmployeeController@insertEmployee');
    Route::get('/get/all', 'EmployeeController@getAllEmployees');
    Route::get('/get/{id}', 'EmployeeController@getEmployeeById');
});

Route::group(['prefix' => 'schedule', 'middleware' => 'auth:sanctum'], function () {
   Route::post('/insert', 'ScheduleController@store');
   Route::get('/get', 'ScheduleController@getSchedulesByCompany');
   Route::post('/check', 'ScheduleController@getFreeEmployees');
   Route::get('/get/today', 'ScheduleController@getTodaySchedules');
});

Route::group(['prefix' => 'v1', 'middleware' => 'auth:sanctum'], function() {
    Route::get('/user/intent-setup-intent', 'SubscriptionController@getSetupIntent');
    Route::put('/user/subscription', 'SubscriptionController@updateSubscription');
    Route::post('/user/payments', 'SubscriptionController@postPaymentMethods');
    Route::get('/user/payment-methods', 'SubscriptionController@getPaymentMethods');
    Route::post('/user/remove-payment', 'SubscriptionController@removePaymentMethod');
    Route::get('/user/cancel-subscription', 'SubscriptionController@removeSubscription');
    Route::get('/user/resume-subscription', 'SubscriptionController@resumeSubscription');
});

Route::group(['prefix' => 'customer', 'middleware' => 'auth:sanctum'], function () {
    Route::get('/get', 'CustomerController@getCustomers');
    Route::get('/get/name', 'CustomerController@getCustomersByName');
    Route::post('/insert', 'CustomerController@store');
    Route::get('/cars/{id}', 'CustomerController@getCustomerCars');
    Route::get('/get/estimates', 'CustomerController@getCustomersWithEstimates');
});

Route::group(['prefix' => 'employee', 'middleware' => 'auth:sanctum'], function () {
    Route::get('/technician/get', 'EmployeeController@getTechnicians');
    Route::get('/all', 'EmployeeController@getAllEmployees');
    Route::get('/schedules/all', 'EmployeeController@employeeSchedules');
});

Route::group(['prefix' => 'car', 'middleware' => 'auth:sanctum'], function () {
    Route::post('/insert', 'CarController@store');
    Route::post('/extern/fetch/data/vin', 'CarController@fetchCarDetailsDataByVin');
});

Route::group(['prefix' => 'part', 'middleware' => 'auth:sanctum'], function () {
    Route::post('/search', 'PartController@search');
    Route::get('/get', 'PartController@getCompanyParts');
    Route::post('/insert', 'PartController@store');
    Route::put('/update', 'PartController@update');
    Route::delete('/delete/{id}', 'PartController@delete');
});

Route::post('/export/parts', 'PartController@exportParts')->middleware('auth:sanctum');
Route::post('/import/parts', 'PartController@importParts')->middleware('auth:sanctum');


Route::post('/user/rfid/attend', 'TimeClockController@store');

Route::post('/password/change', 'Utilities\ChangePasswordController@store')->middleware('auth:sanctum');

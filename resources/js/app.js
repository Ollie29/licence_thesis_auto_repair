/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/MyButton.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('subscription-management', require('./components/SubscriptionManagement').default);
Vue.component('cancel-subscription', require('./components/CancelSubscription').default);
Vue.component('employees-management',require('./components/EmployeesManagement').default);
Vue.component('schedule-component',require('./components/Schedule/ScheduleComponent').default);
Vue.component('timeclocks-component',require('./components/Timeclocks/TimeclocksComponent').default);
Vue.component('change-password-component', require('./components/ChangePasswordComponent').default);
Vue.component('parts-component', require('./components/Inventory/PartsComponent').default);
Vue.component('user-management-component', require('./components/admin/UserManagementComponent').default);
Vue.component('workflow-management-component', require('./components/workflow/WorkflowManagementComponent').default);
Vue.component('order-management-component', require('./components/Order/OrderManagementComponent').default)
Vue.component('home-component', require('./components/home/HomeComponent').default)
Vue.component('reports-component', require('./components/Reports/ReportsComponent').default)
Vue.component('technician-dashboard-component', require('./components/TechnicianDashboardComponent').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});

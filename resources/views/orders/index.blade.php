@extends('layouts.app')

@section('content')

    <order-management-component :creating-user="{{ auth()->user()->employee }}"
                                :countries="{{$countries}}"
                                :logged-user-company="{{auth()->user()->employee->company->id}}">

    </order-management-component>



@endsection

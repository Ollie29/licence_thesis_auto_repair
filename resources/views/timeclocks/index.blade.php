@extends('layouts.app')

@section('content')

<timeclocks-component :employees="{{ json_encode($companyEmployees) }}"
                      :api-token="{{ json_encode($tokenToSend) }}"
                      :clocks="{{ json_encode($clocks) }}"
                      :logged-user-company="{{ auth()->user()->employee->company->id  }}">
</timeclocks-component>

@endsection

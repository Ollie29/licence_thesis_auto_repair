<!doctype html>
<html>
<head>

{{--    https://www.sparksuite.com/open-source/invoice.html--}}{{-- MIT LICENSE--}}
    <style>
        .invoice-box {
            max-width: 800px;
            margin: auto;
            padding: 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0, .15);
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }

        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
        }

        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }

        .invoice-box table tr td:nth-child(2) {
            text-align: right;
        }

        .invoice-box table tr.top table td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.top table td.title {
            font-size: 45px;
            line-height: 45px;
            color: #333;
        }

        .invoice-box table tr.information table td {
            padding-bottom: 40px;
        }

        .invoice-box table tr.heading td {
            background: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
        }

        .invoice-box table tr.details td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.item td{
            border-bottom: 1px solid #eee;
        }

        .invoice-box table tr.item.last td {
            border-bottom: none;
        }

        .invoice-box table tr.total td:nth-child(2) {
            border-top: 2px solid #eee;
            font-weight: bold;
        }

        @media only screen and (max-width: 600px) {
            .invoice-box table tr.top table td {
                width: 100%;
                display: block;
                text-align: center;
            }

            .invoice-box table tr.information table td {
                width: 100%;
                display: block;
                text-align: center;
            }
        }

    </style>
</head>

<body>
<div class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        <tr class="top">
            <td colspan="2">
                <table>
                    <tr>
                        <td class="title">
                            <img src="{{ $image }}" style="max-width:120px;">
                        </td>

                        <td>
                            Invoice #: {{ $estimate->id }}<br>
                            Created: {{ $estimate->created_at }}<br>
                            Copyright &copy; Autoshopsoftware
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="information">
            <td colspan="2">
                <table>
                    <tr>
                        <td>
                            {{$estimate->company_id->name}}<br>
                            {{$estimate->company_id->zip_code}} {{$estimate->company_id->address1}} {{$estimate->company_id->address2}}<br>
                            {{$estimate->company_id->city}}, {{$estimate->company_id->country->name}}
                        </td>

                        <td>
                            {{$estimate->customer_id->company}}<br>
                            {{$estimate->customer_id->first_name}} {{$estimate->customer_id->last_name}}<br>
                            {{$estimate->customer_id->phone}}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="heading">
            <td>
                Car details
            </td>

            <td>
                {{$estimate->car_id->make}} {{$estimate->car_id->model}}
            </td>
        </tr>

        <tr class="details">
            <td>
                {{$estimate->car_id->VIN}}
            </td>

            <td>
                {{$estimate->car_id->license_plate}} {{$estimate->car_id->mileage}}
            </td>
        </tr>

        <tr class="heading">
            <td>
                Parts
            </td>

            <td>
                Price
            </td>
        </tr>

        @foreach($estimate->parts as $part)
        <tr class="item">
            <td>
                {{$part->part_desc}}
            </td>

            <td>
                ({{$part->pivot->qty}}) = ${{$part->retail_price * $part->pivot->qty}}
            </td>
        </tr>
        @endforeach

        <tr class="heading">
            <td>
                Services
            </td>

            <td>
                Price
            </td>
        </tr>

        @foreach($estimate->services as $service)
        <tr class="item">
            <td>
                {{ $service->service_name }}
            </td>

            <td>
                ${{ $service->service_fixed_price }}
            </td>
        </tr>
        @endforeach

        <tr class="item last">
            <td>
            </td>

            <td>
            </td>
        </tr>

        <tr class="total">
            <td></td>

            <td>
               ${{$estimate->total}}
            </td>
        </tr>
    </table>
</div>



</body>
</html>

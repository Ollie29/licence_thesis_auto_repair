    <div class="container">
        <img width="128px" height="128px" src="{{$image}}" class="pl-3">
        <div class="row justify-content-center">
            <table class="table table-hover">
                <thead class="thead-dark">
                <tr>
                    <th>#</th>
                    <th>Part Description</th>
                    <th>Part Number</th>
                    <th>Cost</th>
                    <th>Retail</th>
                    <th>Stock</th>
                    <th>Vendor</th>
                    <th>Critical quantity</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 0 ?>
                @if($data)
                @foreach($data as $datum)
                    <tr>
                        <?php $i++ ?>
                        <th>{{ $i }}</th>
                        <td>{{ $datum->part_desc }}</td>
                        <td>{{ $datum->part_number }}</td>
                        <td>{{ $datum->unit_price }}</td>
                        <td>{{ $datum->retail_price }}</td>
                        <td>{{ $datum->stock }}</td>
                        <td>{{ $datum->vendor }}</td>
                        <td>{{ $datum->critical_quantity }}</td>
                    </tr>
                @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>

@extends('layouts.app')

@section('content')

    <workflow-management-component :estimates="{{ $estimates }}">

    </workflow-management-component>

@endsection

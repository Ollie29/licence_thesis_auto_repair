@extends('layouts.app')

@section('content')

    <div class="bg-light border-right position-fixed">
        <div class="list-group list-group-flush">
            @if(auth()->user()->employee->company->image)
            <img width="128px" height="128px" src="{{ auth()->user()->employee->company->image->url }}" class="img-fluid">
            @endif
            <a href="{{ route('parts.index') }}" class="list-group-item list-group-item-action bg-light">
                <i class="fas fa-cogs"></i> Parts
            </a>
        </div>
    </div>

    <parts-component :parts="{{ json_encode($parts) }}"
                     :api-token="{{ json_encode($tokenToSend) }}">

    </parts-component>

@endsection

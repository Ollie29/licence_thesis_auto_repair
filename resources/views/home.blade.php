@extends('layouts.app')

@section('content')
{{--<div class="container">--}}
{{--    <div class="row justify-content-center">--}}
{{--        <div class="col-md-8">--}}
{{--            <div class="card">--}}
{{--                <div class="card-header">Dashboard</div>--}}

{{--                <div class="card-body">--}}
{{--                    @if (session('status'))--}}
{{--                        <div class="alert alert-success" role="alert">--}}
{{--                            {{ session('status') }}--}}
{{--                        </div>--}}
{{--                    @endif--}}

{{--                        @if(Auth::user()->role === "FREETRIAL")--}}
{{--                            You are logged in as free trial!--}}
{{--                            @endif--}}

{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}

    @if(auth()->user()->role != 'TECHNICIAN')
    <home-component>

    </home-component>
    @endif

    @if(auth()->user()->role == 'TECHNICIAN')
    <technician-dashboard-component>

    </technician-dashboard-component>
    @endif
@endsection

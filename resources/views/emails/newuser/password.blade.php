<p>Hello!</p>
<p>An account has been generated for you, with this email address on Autoshopsoftware©</p>
<p>The password for your new account is {{ $password }}</p>
<p>Feel free to change it once you log in!</p>

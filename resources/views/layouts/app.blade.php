<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon setup -->
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://js.stripe.com/v3/"></script>
    <script src="https://kit.fontawesome.com/d9b0a6ad53.js" crossorigin="anonymous"></script>

    <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/theme.styles.css') }}" rel="stylesheet">

    <script
        src="https://code.jquery.com/jquery-3.5.1.js"
        integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
        crossorigin="anonymous"></script>

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand m-0" href="{{ url('/home') }}" style="width: 18%;">
                    <img src="{{asset('./images/work.png')}}" alt="logo" style="width: 12%; height: auto; padding-bottom: 5px;">
                    Auto shop software
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>


                <div class="collapse navbar-collapse" id="navbarSupportedContent">

                    @if(Auth::check() and (Auth::user()->role === "FREETRIAL" or Auth::user()->role === "ADMIN"))
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link pb-2" href="/calendar">Schedule</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link pb-2" href="/timeclocks">Time Clocks</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link pb-2" href="/inventory/parts">Inventory</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link pb-2" href="/reports/customers">Reports</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link pb-2" href="/workflow">Workflow</a>
                        </li>
                    </ul>
                        @endif


                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->Employee->first_name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                                    <a class="dropdown-item" href="{{route('company.index')}}">
                                        {{__('Company info')}}
                                    </a>

                                    <a class="dropdown-item" id="personalInfoTag" href="#" data-toggle="modal" data-target="#personalInfoModal">
                                        {{__('Change account password')}}
                                    </a>

                                    <a class="dropdown-item" href="/plans" >
                                        {{__('Billing')}}
                                    </a>

                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>

                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <div class="d-flex">
            @yield('content')
        </div>

        @auth
        <change-password-component :api-token="{{ json_encode(App\User::createAPIToken()) }}" :errors="{{ $errors }}" id="my-vue-component">

        </change-password-component>
        @endauth

        <footer id="myFooter" class="navbar navbar-fixed-bottom my-container-color justify-content-center">
            Copyright &copy; Autoshopsoftware
        </footer>


    </div>

</body>

<script>

    $(document).ready(function() {
        if ($("body").height() <= $(window).height()) { //if browser window has vertical scroll
            $('#myFooter').removeClass('navbar navbar-fixed-bottom my-container-color justify-content-center');
            $('#myFooter').addClass('navbar fooooter justify-content-center pt-0 pb-0');
        }
    });

    $('#personalInfoTag').click(function () {
       $('my-vue-component').show();
    });

</script>

</html>

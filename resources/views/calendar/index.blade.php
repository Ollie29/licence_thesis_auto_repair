@extends('layouts.app')

@section('content')


    <schedule-component :api-token="{{ json_encode($tokenToSend) }}"
                        :countries="{{ json_encode($countries) }}">

    </schedule-component>


    @endsection

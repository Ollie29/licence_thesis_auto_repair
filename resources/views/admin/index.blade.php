@extends('admin.app')

@section('admin-content')


    <div class="bg-light border-right position-fixed">
        <div class="list-group list-group-flush shadow">
            <a href="{{ route('admin.index') }}" class="list-group-item list-group-item-action bg-light">
                <i class="fas fa-users"></i> Users
            </a>
            <a href="#" class="list-group-item list-group-item-action bg-light">
                <i class="fas fa-building"></i> Companies
            </a>
        </div>
    </div>


    <user-management-component  :api-token="{{json_encode($tokenToSend)}}"
                                :my-users="{{ json_encode($boss) }}">
    </user-management-component>

@endsection

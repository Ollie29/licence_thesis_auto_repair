@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            @if((Auth::user()->subscribed('Auto shop')) === false)
            <subscription-management :logged-user="{{ json_encode(Auth::user()->email) }}"
                                     :api-token="{{ json_encode($tokenToSend) }}">
            </subscription-management>
            @else
                <cancel-subscription :subscription-status="{{ json_encode(Auth::user()->subscribed('Auto shop')) }}"
                                     :api-token="{{ json_encode($tokenToSend) }}"
                                     :end-date="{{ json_encode($endPeriod) }}">
                </cancel-subscription>
                @endif
        </div>
    </div>

@endsection

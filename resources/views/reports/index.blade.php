@extends('layouts.app')

@section('content')

    <div class="bg-light border-right position-fixed">
        <div class="list-group list-group-flush align-content-center">
            @if(auth()->user()->employee->company->image)
            <img width="128px" height="128px" src="{{ auth()->user()->employee->company->image->url }}" class="img-fluid">
            @endif
            <a href="{{ route('report.index') }}" class="list-group-item list-group-item-action bg-light">
                 Summary by Customer
            </a>
        </div>
    </div>

 <reports-component>

 </reports-component>


@endsection


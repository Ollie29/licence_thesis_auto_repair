@extends('layouts.app')

@section('content')

    <div class="bg-light border-right">
        <div class="list-group list-group-flush">
            @if(auth()->user()->employee->company->image)
            <img width="128px" height="128px" src="{{ auth()->user()->employee->company->image->url }}" class="img-fluid">
            @endif
            <a href="{{ route('company.index') }}" class="list-group-item list-group-item-action bg-light">
                <i class="far fa-building"></i> Company
            </a>
            <a href="{{ route('employee.index') }}" class="list-group-item list-group-item-action bg-light">
                <i class="fas fa-users"></i> Employees
            </a>
        </div>
    </div>


    <employees-management :employees="{{ json_encode($companyEmployees) }}"
                              :api-token="{{ json_encode($tokenToSend) }}">
    </employees-management>

{{--        <div class="row justify-content-center">--}}
{{--            <table class="table table-hover">--}}
{{--                <thead class="thead-dark">--}}
{{--                <tr>--}}
{{--                    <th>#</th>--}}
{{--                    <th>First name</th>--}}
{{--                    <th>Last name</th>--}}
{{--                    <th>Phone</th>--}}
{{--                    <th>Email</th>--}}
{{--                    <th>Role</th>--}}
{{--                    <th>Rate</th>--}}
{{--                    <th>Creation date</th>--}}
{{--                </tr>--}}
{{--                </thead>--}}
{{--                <tbody>--}}
{{--                <?php $i = 0 ?>--}}
{{--                @if($companyEmployees)--}}
{{--                @foreach($companyEmployees as $employee)--}}
{{--                    <tr data-toggle="modal" data-target="#rowModal" >--}}
{{--                        <?php $i++ ?>--}}
{{--                        <th>{{ $i }}</th>--}}
{{--                        <td>{{ $employee->first_name }}</td>--}}
{{--                        <td>{{ $employee->last_name }}</td>--}}
{{--                        <td>{{ $employee->phone }}</td>--}}
{{--                        <td>{{ $employee->user->email }}</td>--}}
{{--                        <td>{{ $employee->user->role }}</td>--}}
{{--                        <td>{{ $employee->price_rate }}</td>--}}
{{--                        <td>{{ $employee->user->created_at }}</td>--}}
{{--                    </tr>--}}
{{--                @endforeach--}}
{{--                    @endif--}}
{{--                </tbody>--}}
{{--            </table>--}}
{{--        </div>--}}

{{--        <div class="modal fade" id="rowModal">--}}
{{--            <div class="modal-dialog">--}}
{{--                <div class="modal-content">--}}
{{--                    <div class="modal-header">--}}
{{--                        <h2 class="modal-title">Please confirm</h2>--}}
{{--                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>--}}
{{--                    </div>--}}
{{--                    <div class="modal-body">--}}
{{--                        <p>This is the modal body. Nice, huh ? </p>--}}
{{--                    </div>--}}
{{--                    <div class="modal-footer">--}}
{{--                        <button type="button" class="btn user-button">Confirm</button>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="row">--}}
{{--            <div class="col">--}}
{{--                <div class="alert alert-success alert-dismissible fade show" role="alert">--}}
{{--                    <button class="close" type="button" data-dismiss="alert"><span>&times;</span></button>--}}
{{--                    <h2 class="alert-heading">This is an alert!</h2>--}}
{{--                    <p>--}}
{{--                        I'm an alert!--}}
{{--                        <a href="#" class="alert-link">Click me!</a>--}}
{{--                    </p>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}



@endsection

@extends('layouts.app')

@section('content')

    <div class="bg-light border-right position-fixed">
        <div class="list-group list-group-flush">
            @if(auth()->user()->employee->company->image)
            <img width="128px" height="128px" src="{{ auth()->user()->employee->company->image->url }}" class="img-fluid">
            @endif
            <a href="{{ route('company.index') }}" class="list-group-item list-group-item-action bg-light">
                <i class="far fa-building"></i> Company
            </a>
            <a href="{{ route('employee.index') }}" class="list-group-item list-group-item-action bg-light">
                <i class="fas fa-users"></i> Employees
            </a>
        </div>
    </div>


    <div class="container pt-5 w-auto pb-4">
        <div class="row">
            <h2>General company information</h2>
        </div>

        <div class="row pt-4">
            <div class="container bg-white text-dark border rounded pb-3">
                <div class="row pl-3 pt-3">
                    <h4>Company info</h4>
                </div>
                <form action="/company/update" method="POST" enctype="multipart/form-data">
                    @method('PUT')
                    @csrf
                    <div class="form-row">
                        <div class="form-group col-6">
                            <label for="companyName" class="col-4 col-form-label">Company Name</label>
                            <div class="col-8">
                                <input id="companyName" name="companyName" value="{{ $currentCompany->name }}" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-6">
                            <label for="image" class="col-4 col-form-label">Company Logo</label>
                            <div class="col-9">
                                <input type="file" name="image" id="image">
                            </div>
                            @if($currentImage == null)
                                <p class="pl-3">Please select the company's logo!</p>
                            @else
                                <img width="96px" height="96px" src="{{$currentImage}}" class="pl-3">
                                @endif
                            <p class="pl-3">Your logo will appear on invoices or reports!</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="vatNumber" class="col-4 col-form-label">VAT Number</label>
                        <div class="col-8">
                            <input id="vatNumber" name="vatNumber" value="{{ $currentCompany->vat_number }}" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="regNumber" class="col-4 col-form-label">Registration Number</label>
                        <div class="col-8">
                            <input id="regNumber" name="regNumber" value="{{ $currentCompany->registration_number }}" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="bankName" class="col-4 col-form-label">Bank Name</label>
                        <div class="col-8">
                            <input id="bankName" name="bankName" value="{{ $currentCompany->bank_name }}" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="iban" class="col-4 col-form-label">IBAN</label>
                        <div class="col-8">
                            <input id="iban" name="iban" value="{{ $currentCompany->iban }}" type="text" class="form-control">
                        </div>
                    </div>
                    <hr>
                    <div class="row pl-3 pt-1">
                        <h4>Address</h4>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-6">
                            <label for="address1" class="col-4 col-form-label">Address 1</label>
                            <div class="col-8">
                                <input id="address1" name="address1" value="{{ $currentCompany->address1 }}" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group col-6">
                            <label for="address2" class="col-4 col-form-label">Address 2</label>
                            <div class="col-8">
                                <input id="address2" name="address2" value="{{ $currentCompany->address2 }}" type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-4">
                            <label for="city" class="col-4 col-form-label">City</label>
                            <div class="col-8">
                                <input id="city" name="city" value="{{ $currentCompany->city }}" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group col-4">
                            <label for="zipCode" class="col-4 col-form-label">ZipCode</label>
                            <div class="col-8">
                                <input id="zipCode" name="zipCode" value="{{ $currentCompany->zip_code }}" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group col-4">
                            <label for="country" class="col-4 col-form-label">Country</label>
                            <div class="col-8">

                                <select id="country" name="country" class="custom-select">
                                    @foreach($countries as $country)
                                        <option value="{{ $country }}" {{ ($currentCountry == $country) ? 'selected' : '' }}>{{ $country }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row justify-content-end">
                        <div class="col-2">
                            <button name="submit" type="submit" class="btn user-button">Confirm</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @endsection

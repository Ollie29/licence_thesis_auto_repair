<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $guarded = ['id'];

    public function estimates() {
        return $this->belongsToMany(Estimate::class, 'estimate_service');
    }

    public function company() {
        return $this->belongsTo(Company::class);
    }


}

<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\NexmoMessage;
use Illuminate\Notifications\Notification;

class ScheduleConfirmationNotification extends Notification
{
    use Queueable;
    protected $customer;
    protected $company;
    protected $startDate;
    protected $endDate;
    protected $content;

    /**
     * ScheduleConfirmationNotification constructor.
     * @param $customer
     * @param $company
     * @param $startDate
     * @param $endDate
     */
    public function __construct($customer, $company, $startDate, $endDate)
    {
        $this->customer = $customer;
        $this->company = $company;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['nexmo'];
    }

    public function toNexmo($notifiable)
    {
        return (new NexmoMessage)
            ->content(
                'Hi, ' . $this->customer->first_name . '!' . '\n' . 'Your appointment has been scheduled on'
            . $this->startDate . ' until ' .$this->endDate . ' . ' . '\n\n' . 'Location \n' . $this->company->name . '\n' .
            $this->company->address . ', ' . $this->company->city . '.')
            ->from('3333');
    }
}

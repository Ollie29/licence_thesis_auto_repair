<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\NexmoMessage;
use Illuminate\Notifications\Notification;

class InvoiceNotification extends Notification
{
    use Queueable;
    protected $customer;
    protected $company;
    protected $startDate;
    protected $car;
    protected $estimate;

    /**
     * Create a new notification instance.
     *
     * @param $customer
     * @param $company
     * @param $startDate
     * @param $car
     * @param $estimate
     */
    public function __construct($customer, $company, $startDate, $car, $estimate)
    {
        $this->customer = $customer;
        $this->company = $company;
        $this->startDate = $startDate;
        $this->car = $car;
        $this->estimate = $estimate;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['nexmo'];
    }

    public function toNexmo($notifiable)
    {
        return (new NexmoMessage)
            ->content(
                'Hi, ' . $this->customer->first_name . '!' . '\n' . 'An invoice has been issued on your name, for services done on ' .
            'car with VIN: ' . $this->car->VIN . '. ' . 'Total issued: ' . $this->estimate->total . '   @' . $this->company->name)
            ->from('3333');
    }

}

<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\NexmoMessage;
use Illuminate\Notifications\Notification;
use App\Clock;

class RFIDNotification extends Notification
{
    use Queueable;
    protected $clock;

    /**
     * Create a new notification instance.
     *
     * @param $clock
     */
    public function __construct($clock)
    {
        $this->clock = $clock;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['nexmo'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
           'RFID uid' => $this->clock->clockIn
        ]);
    }

    public function toNexmo($notifiable)
    {
        return (new NexmoMessage)
            ->content('RFID card with UID: '.$this->clock->clockIn.' has been scanned. Message sent by Autoshopsoftware. Good day!')
            ->from('3333');
    }
}

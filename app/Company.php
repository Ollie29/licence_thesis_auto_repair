<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $guarded = ['id'];

    public function country() {
        return $this->belongsTo(Country::class);
    }

    public function employees() {
        return $this->hasMany(Employee::class);
    }

    public function image() {
        return $this->hasOne(Image::class);
    }

    public function schedules() {
        return $this->hasMany(Schedule::class);
    }

    public function parts() {
        return $this->hasMany(Part::class);
    }

    public function customers() {
        return $this->hasMany(Customer::class);
    }

    public function services() {
        return $this->hasMany(Service::class);
    }

    public function estimates() {
        return $this->hasMany(Estimate::class);
    }
}

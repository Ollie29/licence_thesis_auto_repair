<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Customer extends Model
{
    use Notifiable;

    protected $guarded = [
        'id',
    ];

    public function country() {
        return $this->belongsTo(Country::class);
    }

    public function schedules() {
        return $this->belongsTo(Schedule::class);
    }


     public function routeNotificationForNexmo($notification) {
            return $this->phone;
    }

    public function company() {
        return $this->belongsTo(Company::class);
    }

    public function cars() {
        return $this->hasMany(Car::class);
    }

    public function estimates() {
        return $this->hasMany(Estimate::class);
    }

}

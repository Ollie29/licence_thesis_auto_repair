<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Clock extends Model
{
    protected $table = "clocks";
    protected $guarded = ['id'];
    public $timestamps = false;


    public function employee() {
        return $this->belongsTo(Employee::class);
    }

}

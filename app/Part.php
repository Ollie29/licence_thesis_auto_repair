<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Part extends Model
{
    protected $guarded = ['id'];


    public function company() {
        return $this->belongsTo(Company::class);
    }

    public function estimates() {
        return $this->belongsToMany(Estimate::class, 'estimate_part')->withPivot('qty');
    }
}

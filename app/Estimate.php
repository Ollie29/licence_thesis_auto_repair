<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estimate extends Model
{
    protected $guarded = ['id'];
    protected $table = 'estimates';
    public $timestamps = false;


    public function customer() {
        return $this->belongsTo(Customer::class);
    }

    public function car() {
        return $this->belongsTo(Car::class);
    }

    public function parts() {
        return $this->belongsToMany(Part::class, 'estimate_part')->withPivot('qty');
    }

    public function services() {
        return $this->belongsToMany(Service::class, 'estimate_service');
    }

    public function company() {
        return $this->belongsTo(Company::class);
    }

    public function schedules() {
        return $this->hasMany(Schedule::class);
    }

}

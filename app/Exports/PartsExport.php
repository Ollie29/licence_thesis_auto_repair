<?php

namespace App\Exports;

use App\Part;
use Maatwebsite\Excel\Concerns\FromCollection;

class PartsExport
{
    private $exportStrategy;

    public function __construct(ExportInterface $exportStrategy) {
        $this->exportStrategy = $exportStrategy;
    }

    public function export($companyID) {
        return $this->exportStrategy->export($companyID);
    }
}

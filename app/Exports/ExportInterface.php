<?php


namespace App\Exports;
use App\Part;

interface ExportInterface //Strategy pattern
{
    public function export($companyID);
}

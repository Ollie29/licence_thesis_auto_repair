<?php


namespace App\Exports;


use App\Company;
use App\Part;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class XLSXExport implements ExportInterface, FromCollection, ShouldAutoSize, WithMapping, WithHeadings, WithEvents
{
    private $toCollect;

    /**
     * XLSXExport constructor.
     * @param $toCollect
     */
    public function __construct($toCollect)
    {
        $this->toCollect = $toCollect;
    }

    public function collection()
    {
        return $this->toCollect;
    }

    public function export($companyID) {
        $filename = Carbon::now()->format('Ymdhms').'companyID'.$companyID.'-parts.xlsx';
        Excel::store($this, $filename, 's3', null, ['visibility' => 'public']);
        return $filename;
    }

    public function map($part): array {
        if($part->created_at) {
            return [
                $part->part_desc,
                $part->part_number,
                $part->unit_price,
                $part->retail_price,
                $part->stock,
                $part->vendor,
                $part->critical_quantity,
                Date::dateTimeToExcel($part->created_at),
                Date::dateTimeToExcel($part->updated_at)
            ];
        } else {
            return [
                $part->part_desc,
                $part->part_number,
                $part->unit_price,
                $part->retail_price,
                $part->stock,
                $part->vendor,
                $part->critical_quantity
            ];
        }

    }

    public function headings(): array
    {
        return [
            'Part description',
            'Part number',
            'Unit price',
            'Retail price',
            'Stock',
            'Vendor',
            'Critical Quantity',
            'Added at',
            'Modified at'
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->getStyle('A1:I1')->applyFromArray([
                    'font' => ['bold' => true]
                ]);
            }
        ];
    }
}

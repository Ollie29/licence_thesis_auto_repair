<?php


namespace App\Exports;

use App\Company;
use Barryvdh\DomPDF\Facade;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Facades\Excel;

class PDFExport implements ExportInterface
{
    private $toCollect;

    /**
     * PDFExport constructor.
     * @param $toCollect
     */
    public function __construct($toCollect)
    {
        $this->toCollect = $toCollect;
    }

    public function export($companyID) {
        $filename = Carbon::now()->format('Ymdhms').'companyID'.$companyID.'-parts.pdf';
        $company = Company::find($companyID);
        $image = $company->image->url;
        $data = $this->toCollect;
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('exports.export', compact('data', 'image'));
        Storage::put($filename, $pdf->output(), ['visibility' => 'public']);

        return $filename;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $guarded = ['id'];

    public function customer() {
        return $this->belongsTo(Customer::class);
    }

    public function company() {
        return $this->belongsTo(Company::class);
    }

    public function employee() {
        return $this->belongsTo(Employee::class);
    }

    public function estimate() {
        return $this->belongsTo(Estimate::class);
    }

}

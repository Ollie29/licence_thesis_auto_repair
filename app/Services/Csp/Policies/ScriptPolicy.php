<?php


namespace App\Services\Csp\Policies;
use Spatie\Csp\Exceptions\InvalidDirective;
use Spatie\Csp\Exceptions\InvalidValueSet;
use Spatie\Csp\Keyword;
use Spatie\Csp\Policies\Basic;
use Spatie\Csp\Directive;

class ScriptPolicy extends Basic
{
    public function configure()
    {
        parent::configure();
        $this->addDirective(Directive::SCRIPT, 'js.stripe.com');
    }


}

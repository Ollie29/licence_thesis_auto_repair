<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class TimeSent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $uid;
    public $companyId;

    /**
     * Create a new event instance.
     *
     * @param $uid
     * @param $id
     */
    public function __construct($uid, $id)
    {
        $this->uid = $uid;
        $this->companyId = $id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('time.'.$this->companyId);
    }

    public function broadcastWith() {
        return ['message' => $this->uid . ' for company ' . $this->companyId];
    }
}

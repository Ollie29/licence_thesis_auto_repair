<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PartStockLow implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $companyId;
    public $part;

    /**
     * Create a new event instance.
     *
     * @param $companyId
     * @param $part
     */
    public function __construct($companyId, $part)
    {
        $this->companyId = $companyId;
        $this->part = $part;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('partsStock.'.$this->companyId);
    }

    public function broadcastWith() {
        return ['message' => 'Stock nearly empty for ' . $this->part->part_desc . '->' . $this->part->part_number . '!'];
    }
}

<?php


namespace App\Mail;


use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendNewUserPasswordMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $password;

    /**
     * SendNewUserPasswordMail constructor.
     * @param $password
     */
    public function __construct($password)
    {
        $this->password = $password;
    }

    public function build() {
        return $this->from('autorepair@software.com')->subject('New technician account on autoshopsoftware')
            ->view('emails.newuser.password')
            ->with(['password' => $this->password]);
    }


}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $guarded = ['id'];

    public function invoices() {
        return $this->hasMany('App\Invoice');
    }

    public function customer() {
        return  $this->belongsTo(Customer::class);
    }

    public function estimates() {
        return $this->hasMany(Estimate::class);
    }
}

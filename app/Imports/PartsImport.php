<?php


namespace App\Imports;


use App\Part;
use Illuminate\Support\Carbon;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class PartsImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
        return new Part([
            'part_desc'     => $row['part_description'],
            'part_number'    => $row['part_number'],
            'unit_price' => $row['unit_price'],
            'retail_price' => $row['retail_price'],
            'stock' => $row['stock'],
            'vendor' => $row['vendor'],
            'critical_quantity' => $row['critical_quantity'],
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'company_id' => auth()->user()->employee->company->id,
        ]);
    }
}

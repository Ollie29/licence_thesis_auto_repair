<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $guarded = ['id'];

    public function services() {
        return $this->hasMany('App\Service');
    }
}

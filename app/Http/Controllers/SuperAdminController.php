<?php

namespace App\Http\Controllers;

use App\Employee;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SuperAdminController extends Controller
{
    /**
     * SuperAdminController constructor.
     */
    public function __construct()
    {
        $this->middleware('SuperAdmin');
    }

    public function index()
    {
        $user = Auth::user();
        $tokenToSend = $user->createAPIToken();
        $employees = [];
        $boss = [];
        $users = User::all();
        $toInsert = new \stdClass();
        foreach ($users as $user) {
            array_push($employees, $user->employee);
        }
        for($i = 0; $i < count($users); $i++){
            if($employees[$i]) { //last employee entry is null because of superadmin account not having an employee entry :@ darn..
                $toInsert->id = $users[$i]->id;
                $toInsert->email = $users[$i]->email;
                $toInsert->role = $users[$i]->role;
                $toInsert->created_at = Carbon::parse($users[$i]->created_at)->format('Y/m/d H:m:s');
                $toInsert->first_name = $employees[$i]->first_name;
                $toInsert->last_name = $employees[$i]->last_name;
                $toInsert->rfid_uid = $employees[$i]->rfid_uid;

                array_push($boss, $toInsert);
            }
            $toInsert = new \stdClass();
        }

        return view('admin.index', compact( 'tokenToSend','boss'));
    }
}

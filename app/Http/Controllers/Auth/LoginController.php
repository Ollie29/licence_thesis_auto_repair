<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
        login as originalLogin;
        logout as originalLogout;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
   // protected $redirectTo = RouteServiceProvider::HOME;
    /**
     * Create a new controller instance.
     *
     * @param Request $request
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
//        $user = User::where('email', $request->email)->first();
//        //create token
//        //store token in db
//        $token = $user->createToken('api-token')->plainTextToken;
//      //  $user->token = $token;
//      //  $user->save();
//        $user->withAccessToken($token);
        return $this->originalLogin($request);
    }

    public function logout(Request $request) {
        auth()->user()->active = 0;
        auth()->user()->save();
        auth()->user()->tokens()->delete();
        return $this->originalLogout($request);
    }

    public function redirectPath()
    {
        $user = Auth::user();
        $role = $user->role;
        $user->active = 1;
        $user->save();
        if($user->subscribed('Auto shop')) {
            $user->subscription('Auto shop')->syncStripeStatus();
            if($user->subscription('Auto shop')->ended()) {
                $user->role = 'FREETRIAL';
                $user->save();
            }
        }
        if(!$user->subscribed('Auto shop')) {
            $user->role = 'FREETRIAL';
            $user->save();
        }
        switch ($role) {
            case 'SUPERADMIN':
                return '/admin/panel';
                break;
            default:
                return '/home';
                break;
        }
    }


    /*public function getApiToken(Request $request) {
        $data = $request->validate([
            'email' => 'required|email'
        ]);
        $user = User::where('email', $request->email)->first();
        if(!$user) {
            return response([
                'message' => 'No user found!'
            ]);
        }

        if(!Auth::check()) {
            return response([
                'message' => 'Unauthenticated from backend!'], 401
            );
        }
        //$token = $user->token;
        $token = $user->currentAccessToken();
        $response = [
            'token' => $token
        ];

        return response($response, 200);
    }*/
}

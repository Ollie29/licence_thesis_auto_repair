<?php

namespace App\Http\Controllers;

use App\Company;
use App\Customer;
use App\Employee;
use App\Mail\SendNewUserPasswordMail;
use App\Schedule;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'role:ADMIN,FREETRIAL'])->except('employeeSchedules');
    }


    public function index(Request $request) {
        $user = Auth::user();
        $tokenToSend = User::createAPIToken();
        $companyEmployees = null;
        try {
            $companyEmployees = $user->employee->company->employees;
            foreach ($companyEmployees as $emp) {
                $emp->user_id = User::find($emp->user_id);
            }
        } catch(\ErrorException $e) {

        } finally {
            return view('company.employees', compact('companyEmployees', 'tokenToSend'));
        }
    }

    public function employeeSchedules() {
        $employee = auth()->user()->employee;
        $employeeId = auth()->user()->employee->id;
        //$schedules = $employee->schedules;

        $today = Carbon::today()->setTime(0, 0, 0)->format( 'Y-m-d H:i:s');
        $tomorrow = Carbon::today()->setTime(0,0, 0)->addDays(1)->format( 'Y-m-d');

        $schedules = Schedule::whereBetween('time_start', [$today, $tomorrow])
            ->where('employee_id', $employeeId)->get();


        $estimates = [];
        foreach($schedules as $schedule) {
            $estimate = $schedule->estimate; //for creating the JSON (DTO)
            $parts = [];
            $services = [];
            $customer = Customer::find( $schedule->customer_id);
            $schedule->customer_id = $customer;
            if($schedule->estimate_id) {
                $parts = $schedule->estimate->parts;
                $services = $schedule->estimate->services;
                $car = $schedule->estimate->car;
                $schedule->estimate = $estimate;
                $schedule->estimate->parts = $parts;
                $schedule->estimate->services = $services;
                $schedule->estimate->car = $car;
            }

            for($i = 0; $i < count($parts); $i++) {
                $schedule->estimate->parts[$i]->qty = $parts[$i]->pivot->qty;
            }

            array_push($estimates, $schedule);
        }

        return response()->json($estimates, 200);
    }

    public function insertEmployee(Request $request) {
        try {
            $this->validate($request, [
                'employee.first_name' => ['required'],
                'employee.role' => ['required'],
                'employee.email' => ['required', 'email'],
                'employee.last_name' => ['required'],
                'employee.phone' => ['required'],
                'employee.price_rate' => ['required']
            ]);
        } catch (ValidationException $e) {
            $data = ['message' => 'Validation error!'];
            return response()->json($data, 500);
        }

        $employee = new Employee;
        $user = new User;
        $company = new Company;
        $callingUser = auth()->user();

        $randomString = Str::random(12);

        $employee->first_name = $request->input('employee.first_name');
        $employee->last_name = $request->input('employee.last_name');
        $employee->phone = $request->input('employee.phone');
        $employee->price_rate = $request->input('employee.price_rate');
        $user->email = $request->input('employee.email');
        $user->role = $request->input('employee.role');
        $user->password = Hash::make($randomString);
        $company = $callingUser->employee->company;
        $user->save();
        $user->employee()->save($employee);
        $employee->company()->associate($company);
        $employee->save();
        Mail::to($user)->send(new SendNewUserPasswordMail($randomString));
    }

    public function updateEmployee(Request $request) {
        try {
            $this->validate($request, [
                'employee.id' => ['required'],
                'employee.first_name' => ['required'],
                'employee.role' => ['required'],
                'employee.email' => ['required', 'email'],
                'employee.last_name' => ['required'],
                'employee.phone' => ['required'],
                'employee.price_rate' => ['required']
            ]);
        } catch (ValidationException $e) {
            $data = ['message' => 'Validation error!'];
            return response()->json($data, 500);
        }

        $id = $request->input('employee.id');
        $employee = Employee::findOrFail($id);
        $user = User::findOrFail($employee->user_id);
        $updatedRole = $request->input('employee.role');
        $updatedEmail = $request->input('employee.email');
        $user->role = $updatedRole;
        $user->email = $updatedEmail;
        $employee->first_name = $request->input('employee.first_name');
        $employee->last_name = $request->input('employee.last_name');
        $employee->phone = $request->input('employee.phone');
        $employee->price_rate = $request->input('employee.price_rate');
        $employee->rfid_uid = $request->input('employee.rfid_uid');
        $employee->save();
        $user->save();
    }

    public function getAllEmployees() {
        $user = Auth::user();
        $companyEmployees = $user->employee->company->employees;
        return response()->json($companyEmployees, 200);
    }

    public function getTechnicians() {
        $technicians = User::where('role', 'TECHNICIAN')->orWhere('role', 'ADMIN')->get();
        $techniciansEmployee = new Collection();
        foreach($technicians as $tech) {
            $techniciansEmployee->push($tech->employee);
        }
        return response()->json($techniciansEmployee, 200);
    }
}

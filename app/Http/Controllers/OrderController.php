<?php

namespace App\Http\Controllers;

use App\Car;
use App\Company;
use App\Country;
use App\Customer;
use App\Estimate;
use App\Events\PartStockLow;
use App\Notifications\InvoiceNotification;
use App\Part;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'role:ADMIN,FREETRIAL']);
    }

    public function index() {
        $countries = collect(Country::all())->pluck('name');
        return view('orders.index', compact('countries'));
    }

    public function payEstimate($id) {
        $estimate = Estimate::find($id);
        $estimate->is_paid = true;
        $estimate->save();

        return response()->json('OK', 200);
    }

    public function getUnpaidInvoices() {
        $invoices = Estimate::where('is_paid', false)->where('company_id', auth()->user()->employee->company->id)->where('is_invoice', true)->get();

        foreach($invoices as $invoice) {
            $services = $invoice->services;
            $invoice->services = $services;
        }

        return response()->json($invoices, 200);
    }

    public function getAllInvoices() {
        $companyId = auth()->user()->employee->company->id;
        $invoices = DB::table('estimates')->where('is_invoice', true)->where('company_id', $companyId)->get();
        return response()->json($invoices, 200);
    }

//    public function edit($id) {
//        $estimate = Estimate::find($id);
//        $this->setProperties($estimate);
//        $parts = $estimate->parts;
//        $estimate->parts = $parts;
//        foreach($estimate->parts as $part) {
//            $part->partsQuantity = $part->pivot->qty;
//        }
//        return view('order.edit', compact('estimate'));
//    }

    public function getInvoice($id) {
        $newEstimate = Estimate::find($id);
        $invoicePath = $newEstimate->invoice;

        if(!$invoicePath) {
            return response()->json([
                'info' => 'Estimate has not been exported!'
            ], 200);
        }

        return response()->json([
            'data' => $invoicePath,
            'message' => 'Invoice successfully exported as pdf.'
        ], 200);
    }

    public function exportEstimate(Request $request) {
        $estimate = Estimate::find($request->id);
        $company = auth()->user()->employee->company;
        $companyId = $company->id;
        $filename = Carbon::now()->format('Ymdhms').'companyID'.$companyId.'-invoice.pdf';
        $company = Company::find($companyId);
        $image = $company->image->url;
        $customer = Customer::find($estimate->customer_id);
        $car = Car::find($estimate->car_id);
        $estimate->customer_id = $customer;
        $estimate->car_id = $car;
        $services = $estimate->services;
        $estimate->services = $services;
        $estimate->company_id = $company;
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('exports.invoice', compact('estimate', 'image'));
        Storage::put($filename, $pdf->output(), ['visibility' => 'public']);

        $fullPath = Storage::disk('s3')->path($filename); // creates and stores to aws s3 .pdf file
        $newPath = Storage::disk('s3')->url($fullPath); // returns entire url path for the file download

        $newEstimate = Estimate::find($request->id);
        $newEstimate->invoice = $newPath;
        $newEstimate->save();

        return response()->json([
            'data' => $newPath,
            'message' => 'Invoice successfully exported as pdf.'
        ], 200);

    }

    public function deleteEstimate($id) {
        $estimate = Estimate::find($id);
        //add parts back to stock //todo
        $parts = $estimate->parts;

        if($estimate) {
            $estimate->delete();
            return response()->json('Successfully deleted!', 200);
        }


        return response()->json('Error in deleting!', 500);
    }

    public function getAll() {
        $estimates = auth()->user()->employee->company->estimates;
        foreach($estimates as $estimate) {
            $this->setProperties($estimate);
        }

        return response()->json($estimates, 200);
    }

    public function setProperties($estimate) {
        $customer = Customer::find($estimate->customer_id);
        $car = Car::find($estimate->car_id);
        $estimate->customer_id = $customer;
        $estimate->car_id = $car;
        $services = $estimate->services;
        $estimate->services = $services;
    }

    public function storeEstimate(Request $request) {
        $currentCompany = auth()->user()->employee->company;
        $parts = $request->parts;
        $services = $request->services;
        $customerId = $request->customer;
        $carId = $request->car;
        $currentTime = $request->current_time;
        $currentEstimate = $request->current_estimate;
        $customer = Customer::find($customerId);
        $car = Car::find($carId);

        $estimate = new Estimate;
        $estimate->parts_total = $currentEstimate['partsTotal'];
        $estimate->services_total = $currentEstimate['servicesTotal'];
        $estimate->total = $currentEstimate['total'];
        $estimate->is_invoice = !$request->is_estimate;
        $estimate->is_paid = $currentEstimate['is_paid'];
        $estimate->created_at = $currentTime;
        $estimate->car()->associate($car);
        $estimate->customer()->associate($customer);
        $estimate->company()->associate($currentCompany);

        $estimate->save();

        for($i = 0; $i < count($parts); $i++) {
            $estimate->parts()->attach($parts[$i]['id'], ['qty' => $parts[$i]['partsQuantity'] ]);
        }

        for($i = 0; $i < count($services); $i++) {
            $estimate->services()->attach($services[$i]['id']);
        }

        if($estimate->is_invoice) {
            for($i = 0; $i < count($parts); $i++) {
                $savedPart = Part::find($parts[$i]['id']);
                $part = $parts[$i];
                if(($savedPart->stock - $part['partsQuantity']) <= ($savedPart->stock)) {
                    return response()->json('Stock for part ' . $savedPart->part_desc . ' is empty!', 500);
                }
                if(($savedPart->stock - $part['partsQuantity']) <= ($savedPart->critical_quantity)) {
                    broadcast(new PartStockLow($currentCompany->id, $savedPart));
                }

                $savedPart->stock = $savedPart->stock - $part['partsQuantity'];
                $savedPart->save();
            }
        }

        if($estimate->is_invoice === true) {
            if ($currentEstimate['send_sms'] === true) {
                $customer->notify(new InvoiceNotification($customer, $currentCompany, $currentTime,
                    $car, $estimate));
            }
        }

        return response()->json($estimate->id, 200);
    }
}

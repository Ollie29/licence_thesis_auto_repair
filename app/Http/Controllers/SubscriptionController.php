<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Cashier\Subscription;

class SubscriptionController extends Controller
{
    /**
     * SubscriptionController constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth','role:ADMIN,FREETRIAL']);
    }

    public function index() {
        $user = User::where('email', auth()->user()->email)->first();
        $tokenToSend = User::createAPIToken();
        $endPeriod = User::getBillingPeriodEnd();
        return view('plans.index', compact('tokenToSend', 'endPeriod'));
    }

    public function getSetupIntent(Request $request) {
        $user = $request->user();
         return $user->createSetupIntent();
    }

    public function postPaymentMethods(Request $request) {
        $user = $request->user();
        $paymentMethodID = $request->get('payment_method');
        if($user->stripe_id == null) {
            $user->createAsStripeCustomer();
        }

        $user->addPaymentMethod($paymentMethodID);
        $user->updateDefaultPaymentMethod($paymentMethodID);

        return response()->json(null, 204);
    }

    public function getPaymentMethods(Request $request){
        $user = $request->user();
        $methods = array();
        if($user->hasPaymentMethod()) {
            foreach($user->paymentMethods() as $method) {
                array_push($methods, [
                    'id' => $method->id,
                    'brand' => $method->card->brand,
                    'last_four' => $method->card->last4,
                    'exp_month' => $method->card->exp_month,
                    'exp_year' => $method->card->exp_year,
                ] );
            }
        }
        return response()->json($methods);
    }

    public function removePaymentMethod(Request $request){
        $user = $request->user();
        $paymentMethodID = $request->get('id');

        $paymentMethods = $user->paymentMethods();
        foreach($paymentMethods as $method) {
            if($method->id == $paymentMethodID) {
                $method->delete();
                break;
            }
        }

        return response()->json(null, 204);
    }

    public function updateSubscription(Request $request){
        $user = $request->user();
        $planID = $request->get('plan');
        $paymentID = $request->get('payment');

        if(!($user->subscribed('Auto shop'))) {
            $user->newSubscription('Auto shop', $planID)->create($paymentID);
        } else {
            $user->subscription('Auto shop')->swap($planID);
        }

        $user->role = 'ADMIN';
        $user->save();

        return response()->json([
            'subscription_updated' => true
        ]);
    }

    public function removeSubscription(Request $request) {
        $user = $request->user();
        $ret = false;

        if($user->subscribed('Auto shop')) {
            $user->subscription('Auto shop')->cancel();
            $user->subscription('Auto shop')->syncStripeStatus();
           // $user->role = 'FREETRIAL';
           // $user->save();
            $ret = true;
        }

        return response()->json([
           'subscription_ended' => $ret
        ]);

    }

    public function resumeSubscription(Request $request) {
        $user = $request->user();
        $ret = false;
        if($user->subscribed('Auto shop', '')) {
            $user->subscription('Auto shop')->resume();
            $user->role = 'ADMIN';
            $user->save();
            $ret = true;
        }

        return response()->json([
            'subscription_resumed' => $ret
        ]);
    }
}

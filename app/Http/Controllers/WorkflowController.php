<?php

namespace App\Http\Controllers;

use App\Car;
use App\Company;
use App\Customer;
use App\Estimate;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;

class WorkflowController extends Controller
{
    /**
     * WorkflowController constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth', 'role:ADMIN,FREETRIAL']);
    }


    public function index() {
        $estimates = auth()->user()->employee->company->estimates;
        foreach($estimates as $estimate) {
            $customer = Customer::find($estimate->customer_id);
            $car = Car::find($estimate->car_id);
            $estimate->customer_id = $customer;
            $estimate->car_id = $car;
            $services = $estimate->services;
            $estimate->services = $services;
        }
        return view('workflow.index', compact('estimates'));
    }


}

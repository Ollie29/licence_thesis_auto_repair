<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Events\TimeSent;
use App\Notifications\RFIDNotification;
use App\User;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Clock as Clock;

class TimeClockController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'role:ADMIN']);
    }

    public function index(Request $request) {
        $user = Auth::user();
        $tokenToSend = User::createAPIToken();
        $companyEmployees = null;
        $clocks = []; //order of clock elements is related directly to the order of employees[]
        $now = Carbon::now()->toDateString();
        try {
            $companyEmployees = $user->employee->company->employees;
        } catch(\ErrorException $e) {

        } finally {
            if($companyEmployees) {
                foreach ($companyEmployees as $emp) {
                    array_push($clocks, $emp->clocks);
                }
            }
            return view('timeclocks.index', compact('companyEmployees', 'tokenToSend', 'clocks'));
        }
    }

    public function store(Request $request) {
        $clockIn = $request->clock_in;
        $clockOut = $request->clock_out;
        $employeeId = $request->employee_id;

        $employee = Employee::find($employeeId);

        $clock = new Clock;
        $clock1 = new Clock;
        if($request->clock_in) { //used when not only clock out is submitted in the form
            $clock->clock_in = $clockIn;
            $clock->employee()->associate($employee);
            $clock->save();
        }

        if($request->uid) { //used when request is sent from RFID reader module
//            Storage::append('arduino-log.txt',
//                'Time: ' . now() . ', '
//                . 'Card UID: ' . $request->uid . '\n'
//            );
            $uid = $request->uid;
            $timestamp = $request->timedate;
            $employee = Employee::where('rfid_uid', $uid)->first();
            $dateDelimited = explode("/", $timestamp);
            $dayDelimited = explode(" ", $dateDelimited[2])[0];
            $timeDelimited = explode(":", $timestamp);
            $hourDelimited = explode(" ", $timeDelimited[0]);
            $newTimestamp = $dateDelimited[0] . '-' . $dateDelimited[1] . '-' . $dayDelimited . ' ' . $hourDelimited[1] . ':' .
                $timeDelimited[1] . ':' . $timeDelimited[2];;
            try {
                $date = new DateTime($newTimestamp);
            } catch (\Exception $e) {
            }
            $timestampToInsert = $date->format('Y-m-d H:i:s');
            $clockOut = $timestampToInsert;

            broadcast(new TimeSent($uid, $employee->company->id)); //websocket event
        }

        $clock1->clock_in = $clockOut;
        $clock1->employee()->associate($employee);
        $clock1->save();

    }

    public function getClocks() {
        $user = Auth::user();
        $companyEmployees = null;
        $clocks = []; //order of clock elements is related directly to the order of employees[]

        $companyEmployees = $user->employee->company->employees;
        foreach($companyEmployees as $emp) {
            array_push($clocks, $emp->clocks);
        }

        return $clocks;
    }

//    public function testing(Request $request) {
//        Storage::append('arduino-log.txt',
//            'Time: ' . now() . ', '
//            . 'Card UID: ' . $request->uid . '\n'
//        );
//        $uid = $request->uid;
//        $timestamp = $request->timedate;
//        $employee = Employee::where('rfid_uid', $uid)->first();
//        $dateDelimited = explode("/", $timestamp);
//        $dayDelimited = explode(" ", $dateDelimited[2])[0];
//        $timeDelimited = explode(":", $timestamp);
//        $hourDelimited = explode(" ", $timeDelimited[0]);
//        $newTimestamp = $dateDelimited[0] . '-' . $dateDelimited[1] . '-' . $dayDelimited . ' ' . $hourDelimited[1] . ':' .
//            $timeDelimited[1] . ':' . $timeDelimited[2];;
//        try {
//            $date = new DateTime($newTimestamp);
//        } catch (\Exception $e) {
//        }
//        $timestampToInsert = $date->format('Y-m-d H:i:s');
//        $clock1 = new Clock;
//        $clock1->clock_in = $timestampToInsert;
//      //  $clock11 = $clock1;
//        $clock1->employee()->associate($employee);
//        $clock1->save();
//    //    $user = User::where('email', 'test@test.com')->first();
//      //  $user->notify(new RFIDNotification($clock11));
//    }
}

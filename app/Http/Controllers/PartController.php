<?php

namespace App\Http\Controllers;

use App\Exports\PartsExport;
use App\Exports\PDFExport;
use App\Exports\XLSXExport;
use App\Imports\PartsImport;
use App\Part;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class PartController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'role:ADMIN,FREETRIAL']);
    }

    public function index() {
        $parts = Part::all();
        $tokenToSend = User::createAPIToken();
        return view('parts.index', compact('parts', 'tokenToSend'));

    }

    public function getCompanyParts() {
        $currentCompany = Auth::user()->employee->company->parts;
        return response()->json($currentCompany, 200);
    }

    public function search(Request $request) {
        $parts = [];
        if($search = $request->search) {
            $parts = Part::where('part_desc', 'LIKE', "%$search%")
                            ->orWhere('part_number', 'LIKE', "%$search%")
                            ->orWhere('vendor', 'LIKE', "%$search%")->get();
        } else {
            $parts = Part::all();
        }
        return response()->json($parts, 200);
    }

    public function exportParts(Request $request)
    {
        $parts = Auth::user()->employee->company->parts;
        $currentCompany = auth()->user()->employee->company->id;

        if($request->choice == 'XLSX') {

        $exportType = new PartsExport(new XLSXExport($parts));
        $fullPath = Storage::disk('s3')->path($exportType->export($currentCompany)); // creates and stores to aws s3 .xlsx file
        $newPath = Storage::disk('s3')->url($fullPath); // returns entire url path for the file download

        return response()->json([
            'data' => $newPath,
            'message' => 'Parts are successfully exported as xlsx.'
        ], 200);
        }
        else if($request->choice == 'PDF') {
            $exportType = new PartsExport(new PDFExport($parts));
            $fullPath = Storage::disk('s3')->path($exportType->export($currentCompany)); // creates and stores to aws s3 .xlsx file
            $newPath = Storage::disk('s3')->url($fullPath); // returns entire url path for the file download


            return response()->json([
                'data' => $newPath,
                'message' => 'Parts are successfully exported as pdf.'
            ], 200);

        } else {
            return response()->json('Format not found', 404);
        }

    }

    public function importParts(Request $request) {
        Excel::import(new PartsImport, request()->file('file'));
        return response()->json('Imported successfully!', 200);
    }

    public function store(Request $request) {
        $part = new Part;
        $part->part_desc = $request->part_desc;
        $part->part_number = $request->part_number;
        $part->unit_price = $request->unit_price;
        $part->retail_price = $request->retail_price;
        $part->stock = $request->stock;
        $part->vendor = $request->vendor;
        $part->critical_quantity = $request->critical_quantity;

        $part->save();

        $currentCompany = Auth::user()->employee->company;

        $currentCompany->parts()->save($part);

        return response()->json($part, 200);
    }

    public function update(Request $request) {
        $part = Part::where('part_number', $request->part_number)
                        ->update([
                            'part_desc' => $request->part_desc,
                            'part_number' => $request->part_number,
                            'unit_price' => $request->unit_price,
                            'retail_price' =>  $request->retail_price,
                            'stock' => $request->stock,
                            'vendor' => $request->vendor,
                            'critical_quantity' => $request->critical_quantity
                        ]);

        return response()->json('Success', 200);
    }

    public function delete(Request $request) {
        $part = Part::where('part_number', $request->id)->first();
        if($part)
            $part->delete();
        return response()->json('Deleted!', 200);
    }


}

<?php

namespace App\Http\Controllers;

use App\Country;
use App\Customer;
use Illuminate\Http\Request;
use function Symfony\Component\String\s;

class CustomerController extends Controller
{
    public function getCustomers() {
        $customers = auth()->user()->employee->company->customers;
        return response()->json($customers, 200);
    }

    public function getCustomersByName(Request $request) {

    }

    public function getCustomersWithEstimates() {
        $company = auth()->user()->employee->company;
        $customers = $company->customers;

        foreach($customers as $customer) {
            $estimates = $customer->estimates;
            $customer->estimates = $estimates;
        }

        return response()->json($customers, 200);
    }

    public function getCustomerCars(Request $request) {
        $cars = Customer::find($request->id)->cars;
        return response()->json($cars, 200);
    }

    public function store(Request $request) {
        $customer = new Customer;
        $company = auth()->user()->employee->company;
        $customer->first_name = $request->first_name;
        $customer->last_name = $request->last_name;
        $customer->phone = $request->phone;
        $customer->address_line1 = $request->address1;
        $customer->address_line2 = $request->address2;
        $customer->email = $request->email;
        $customer->city = $request->city;
        $customer->zip_code = $request->zip_code;
        $customer->tax_exempt = $request->tax_exempt;
        $customer->discount_eligible = $request->discount_eligible;
        $country = Country::where('name', $request->country)->first();
        $customer->country()->associate($country);
        $customer->company()->associate($company);
        $customer->save();

        return response()->json(['message' => 'Customer inserted successfully!'], 200);
    }

}

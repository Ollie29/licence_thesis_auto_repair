<?php

namespace App\Http\Controllers;

use App\Company;
use App\Country;
use App\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class CompanyController extends Controller
{
    /**
     * CompanyController constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth', 'role:ADMIN,FREETRIAL']);
    }

    public function index() {
        $currentCompany = new Company;
        $currentCompany = auth()->user()->employee->company;
        if($currentCompany->country)
            $currentCountry = $currentCompany->country->name;
        else
            $currentCountry = null;
        if($currentCompany->image) {
            $currentImage = $currentCompany->image->url;//Storage::disk('s3')->response('user_logos/' . $currentImage);
        }
        else {
            $currentImage = null;
        }
        $countries = collect(Country::all())->pluck('name');
        return view('company.index', compact(['currentCompany', 'countries', 'currentCountry',
            'currentImage']));
    }

    public function store(Request $request) {
        $currentCompany = new Company;
        $currentCompany = auth()->user()->employee->company;
        if(!$currentCompany->image) {
            $path = $request->file('image')->store('user_logos', 's3');
            Storage::disk('s3')->setVisibility($path, 'public');
            $image = new Image;
        } else {
            $image = $currentCompany->image;
            $path = $request->file('image')->store('user_logos', 's3');
            Storage::disk('s3')->setVisibility($path, 'public');
        }
        $currentCompany->name = $request->companyName;
        $currentCompany->vat_number = $request->vatNumber;
        $currentCompany->registration_number = $request->regNumber;
        $currentCompany->address1 = $request->address1;
        $currentCompany->address2 = $request->address2;
        $currentCompany->bank_name = $request->bankName;
        $currentCompany->iban = $request->iban;
        $currentCompany->city = $request->city;
        $currentCompany->zip_code = $request->zipCode;
        $currentCompany->country_id = Country::where('name', $request->country)->first()->id;

        $image->filename = basename($path);
        $image->url = Storage::disk('s3')->url($path);
        $image->save();
        $currentCompany->save();
        $currentCompany->image()->save($image);
        return redirect()->route('company.index');
    }
}

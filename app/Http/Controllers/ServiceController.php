<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ServiceController extends Controller
{
    public function getAllCompanyServices() {
        $user = auth()->user();
        $services = $user->employee->company->services;
        return response()->json($services, 200);
    }
}

<?php

namespace App\Http\Controllers;

use App\Car;
use App\Customer;
use Illuminate\Http\Request;

class CarController extends Controller
{
    public function store(Request $request) {
        $car = new Car;
        $car->model_year = $request->model_year;
        $car->make = $request->make;
        $car->model = $request->model;
        $car->type = $request->type;
        $car->mileage = $request->mileage;
        $car->color = $request->color;
        $car->license_plate = $request->license_plate;
        $car->vin = $request->vin;
        $car->note = $request->note;
        $car->engine_size = $request->engine_size;
        $car->production_date = $request->production_date;
        $car->transmission = $request->transmission;
        $car->drive_train = $request->drive_train;

        $car->save();
        $customer = Customer::find($request->customer_id);
        $customer->cars()->save($car);

        return response()->json(['message' => 'New car inserted successfully!'], 200);
    }


    public function fetchCarDetailsDataByVin(Request $request) {
        $apiPrefix = "https://api.vindecoder.eu/3.1";
        $apiKey = "c9dbdd4a0c0d";   // Your API key
        $secretKey = "9b9cd77034";  // Your secret key
        $id = "decode";
        $vin = $request->vin;

        $controlSum = substr(sha1("{$vin}|{$id}|{$apiKey}|{$secretKey}"), 0, 10);

        $data = file_get_contents("{$apiPrefix}/{$apiKey}/{$controlSum}/decode/{$vin}.json", false);
        $result = json_decode($data);

        return response()->json($result, 200);
    }
}

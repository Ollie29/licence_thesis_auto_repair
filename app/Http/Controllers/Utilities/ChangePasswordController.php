<?php

namespace App\Http\Controllers\Utilities;

use App\Http\Controllers\Controller;
use App\Rules\OldPassword;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class ChangePasswordController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
        try {
            $request->validate([
                'currentPassword' => ['required', new OldPassword],
                'newPassword' => ['required',]
            ]);
        } catch(ValidationException $e) {
            $data = ['message' => 'Validation error!'];
            return response()->json($data, 500);
        }

        $user = User::where('email', auth()->user()->email)->first();
        $user->password = Hash::make($request->newPassword);
        $user->save();
    }
}

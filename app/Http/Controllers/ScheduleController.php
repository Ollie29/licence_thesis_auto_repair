<?php

namespace App\Http\Controllers;

use App\Country;
use App\Customer;
use App\Employee;
use App\Estimate;
use App\Notifications\ScheduleConfirmationNotification;
use App\Schedule;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ScheduleController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'role:ADMIN,FREETRIAL']);
    }

    public function index() {
        $user = Auth::user();
        $tokenToSend = User::createAPIToken();
        $countries = collect(Country::all())->pluck('name');
        return view('calendar.index', compact('tokenToSend', 'countries'));
    }

    public function getTodaySchedules(Request $request) {
        $today = Carbon::today()->setTime(0, 0, 0)->format( 'Y-m-d H:i:s');
        $tomorrow = Carbon::today()->setTime(0,0, 0)->addDays(1)->format( 'Y-m-d');

        $schedules = DB::table('schedules')->whereBetween('time_start', [$today, $tomorrow])->get();

        return response()->json($schedules, 200);

    }

    public function store(Request $request) {
        $user = Auth::user();
        $currentCompany = $user->employee->company;
        $schedule = new Schedule;
        $schedule->time_start = $request->time_start;
        $schedule->time_end = $request->time_end;
        if($request->customer_id)
        $schedule->employee_id = $request->technician_id;


        if($request->is_invoice == true) {
            $invoice = Estimate::find($request->invoice_id);

            $schedule->estimate()->associate($invoice);
            $schedule->customer_id = $invoice->customer_id;
        }
        $schedule->title = $request->title;
        $schedule->content = $request->special_content;
        $schedule->company()->associate($currentCompany);
        $schedule->save();
        $customer = Customer::find($request->customer_id);

        if($request->sendSMS == true) {
            $customer->notify(new ScheduleConfirmationNotification($customer, $currentCompany, $request->time_start,
                $request->time_end));
        }

        return response()->json(['message' => 'Appointment saved successfully!'], 200);
    }

    public function getSchedulesByCompany() {
        $user = Auth::user();
        $currentCompany = $user->employee->company;
        $schedules = $currentCompany->schedules;
        return response()->json($schedules, 200);
    }

    public function removeEmployeeEntry($entry, $array) {
        for($i = 0 ; $i < count($array); $i++) {
            if($array[$i]['id'] == $entry->id) {
                array_splice($array, $i, 1);

            }
        }
        return $array;
    }

    public function getFreeEmployees(Request $request) {
        $time_start = Carbon::parse($request->time_start);
        $time_end = Carbon::parse($request->time_end);
        $company = auth()->user()->employee->company;
        $todaySchedules = DB::table('schedules')->where('company_id', $company->id)->get();
        $employeesToReturn = auth()->user()->employee->company->employees->toArray();

        foreach($todaySchedules as $schedule) {
            $parsed_start = Carbon::parse($schedule->time_start);
            $parsed_end = Carbon::parse($schedule->time_end);
            $employee = new Employee;
            $employee = Employee::find($schedule->employee_id);

            if($time_start->format('Y-m-d') == $parsed_start->format('Y-m-d')) {

                if (($time_start->format('H:i:s') < $parsed_end->format('H:i:s')) &&
                    $time_end->format('H:i:s') > $parsed_start->format('H:i:s')) {
                    $employeesToReturn = $this->removeEmployeeEntry($employee, $employeesToReturn);
                    continue;
                }
                if (($time_start->format('H:i:s') < $parsed_start->format('H:i:s')) &&
                    ($time_end->format('H:i:s') < $parsed_end->format('H:i:s') &&
                        $time_end->format('H:i:s') > $parsed_start->format('H:i:s'))) {
                    $employeesToReturn = $this->removeEmployeeEntry($employee, $employeesToReturn);
                    continue;
                }
                if (($time_start->format('H:i:s') < $parsed_end->format('H:i:s')) &&
                    ($time_start->format('H:i:s') > $parsed_start->format('H:i:s')) &&
                    ($time_end->format('H:i:s') > $parsed_end->format('H:i:s'))) {
                    $employeesToReturn = $this->removeEmployeeEntry($employee, $employeesToReturn);
                    continue;
                }
            }
        }

        return response()->json($employeesToReturn, 200);
    }


}

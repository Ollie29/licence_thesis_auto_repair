<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'role:ADMIN,FREETRIAL,TECHNICIAN']);
    }

    /**a
     * Show the application dashboard.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
       $user = $request->user();
       if(!($user->role == 'TECHNICIAN')) {
           if ((!$user->subscribed('Auto shop'))) { //user is re set as free trial after subscription period ends
               $user->role = 'FREETRIAL';
               $user->save();
           } else if ($user->subscribedToPlan('Premium', 'Auto shop')) {
               $user->role = 'ADMIN';
               $user->save();
           }
       }
       return view('home');
    }

}

<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Part;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function getAll() {
        $employees = [];
        $boss = [];
        $users = User::all();
        $toInsert = new \stdClass();
        foreach ($users as $user) {
            array_push($employees, $user->employee);
        }
        for ($i = 0; $i < count($users); $i++) {
            if ($employees[$i]) { //last employee entry is null because of superadmin account not having an employee entry :@ darn..
                $toInsert->id = $users[$i]->id;
                $toInsert->email = $users[$i]->email;
                $toInsert->role = $users[$i]->role;
                $toInsert->created_at = Carbon::parse($users[$i]->created_at)->format('Y/m/d H:m:s');
                $toInsert->first_name = $employees[$i]->first_name;
                $toInsert->last_name = $employees[$i]->last_name;
                $toInsert->rfid_uid = $employees[$i]->rfid_uid;

                array_push($boss, $toInsert);
            }
            $toInsert = new \stdClass();
        }
        return $boss;
    }

    public function getAllWrapper() {
        if(auth()->user()->tokenCan('superadmin:token')) {

            $boss = $this->getAll();

            return response()->json($boss, 200);
        }

        return response()->json(['message' => 'Your token is invalid!'], 403);
    }

    public function getUsersUnderDate(Request $request) {
        $user = Auth::user();
        $users = null;
        if($user->tokenCan('superadmin:token')) {
            $users = User::whereDate('created_at', '>', Carbon::now()->subDays($request->days))->get();
            return response()->json($users, 200);
        }
        return response()->json(['message' => 'Your token is invalid!'], 403);
    }

    public function getUsersStatistics(Request $request) {
        $user = Auth::user();
        if($user->tokenCan('superadmin:token')) {
            $activeUsers = User::where('active', 1)->count();
            $inactiveUsers = User::where('active', 0)->count();
            return response()->json([
                'active' => $activeUsers,
                'inactive' => $inactiveUsers
            ], 200);
        }

        return response()->json(['message' => 'Your bearer token is invalid!'], 403);
    }

    public function getUserTypeStatistics() {
        $user = Auth::user();
        if($user->tokenCan('superadmin:token')) {
            $techs = User::where('role', 'TECHNICIAN')->count();
            $freetrials = User::where('role', 'FREETRIAL')->count();
            $admins = User::where('role', 'ADMIN')->count();

            return response()->json([
                'technicians' => $techs,
                'freetrials' => $freetrials,
                'admins' => $admins
            ], 200);
        }
        return response()->json(['message' => 'Your bearer token is invalid!'], 403);
    }

    public function store(Request $request) {

    }

    public function update(Request $request) {
        User::where('id', $request->id)
            ->update([
                'email' => $request->email,
                'role' => $request->role,
            ]);
        $user = User::find($request->id);

        $employee = $user->employee;

        Employee::find($employee->id)->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'rfid_uid' => $request->rfid_uid,
        ]);

        return response()->json('Success', 200);
    }

    public function search(Request $request) {
        $users = [];
        $newUsers = [];
        if($search = $request->search) {
            $users = User::where('id', 'LIKE', "%$search%")
                ->orWhere('email', 'LIKE', "%$search%")
                ->orWhere('role', 'LIKE', "%$search%")->get();
        } else {
            $users = $this->getAll();
        }

        foreach($users as $user) {
            array_push($newUsers, $user->id);
        }
        return response()->json($newUsers, 200);
    }

    public function delete(Request $request) {
        $part = User::where('id', $request->id)->first();
        if($part)
            $part->delete();
        return response()->json('Deleted!', 200);
    }

}

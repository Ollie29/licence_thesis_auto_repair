<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param mixed ...$roles
     * @return mixed
     */
    public function handle($request, Closure $next, ...$roles) // ... spread operator
    {
        if (!Auth::check())
            return redirect('login');

        $user = auth()->user();

        foreach($roles as $role) {
            // Check if user has the role This check will depend on how your roles are set up
            if($user->role === $role)
                return $next($request);
        }

        Auth::logout();
        return redirect(route('welcome'));
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class SuperAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        if($user->role == 'SUPERADMIN'){
            return $next($request);
        }
        Auth::logout();
        return redirect(route('welcome'))->with('status', 'You do not have super admin privileges!');
    }
}

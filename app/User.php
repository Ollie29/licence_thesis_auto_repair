<?php

namespace App;

use App\Notifications\RFIDNotification;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Laravel\Cashier\Billable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use Billable;
    use HasApiTokens;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

   public function employee() {
       return $this->hasOne(Employee::Class);
   }

    public static function createAPIToken() {
        $token = '';
        $user = Auth::user();
        if($user->role == 'SUPERADMIN') {
            $token = $user->createToken('api-token', ['superadmin:token'])->plainTextToken;
        } else {
            $token = $user->createToken('api-token', ['user:token'])->plainTextToken;
        }
        $user->withAccessToken($token);
        return $user->currentAccessToken();
    }

    public static function getBillingPeriodEnd() {
       if(Auth::user()->subscribed('Auto shop')) {
           $subscription = Auth::user()->subscription('Auto shop')->asStripeSubscription();
           return Carbon::createFromTimeStamp($subscription->current_period_end)->format('d/m/Y');
       }
    }

    public function routeNotificationForNexmo($notification)
    {
        return $this->employee->phone;
    }

    public function isSuperAdmin() {
       if($this->role === 'SUPERADMIN')
           return true;
       else
           return false;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $guarded = ['id'];

    public function customers() {
        return $this->hasMany('App\Customers');
    }

    public function companies() {
        return $this->hasMany('App\Country');
    }
}

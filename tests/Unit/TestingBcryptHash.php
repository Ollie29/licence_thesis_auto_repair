<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\TestCase;
use Illuminate\Support\Facades\Hash;
use Tests\CreatesApplication;


class TestingBcryptHash extends TestCase
{
    use CreatesApplication;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testBcryptPasswordHash()
    {
        $this->assertTrue(Hash::check('12345678','$2y$10$1jGuq1yu9hSS.mo.M6OCqO2ZxQLmsWuhad.xvyoDjdRBx97pGTXcq'));
    }
}

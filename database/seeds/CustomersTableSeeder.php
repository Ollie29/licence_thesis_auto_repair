<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('customers')->insert([
            'phone'=> Str::random(10),
            'address_line1'=> Str::random(10).'@Satu Mare',
            'city'=>'satu mare',
            'discount_eligible'=>1
        ]);

    }
}

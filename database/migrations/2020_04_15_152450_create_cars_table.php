<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->id();
            //$table->bigInteger('customer_id');
            $table->string('model_year');
            $table->string('make');
            $table->string('model');
            $table->enum('type', ['Coupe', 'Sedan', 'SUV', 'MPV', 'Truck', 'Van', 'Cabriolet', 'Hatchback']);
            $table->string('mileage')->nullable();
            $table->string('color')->nullable();
            $table->string('license_plate')->nullable();
            $table->string('VIN')->unique();
            $table->string('note')->nullable();
            $table->string('engine_size');
            $table->string('production_date')->nullable();
            $table->enum('transmission', ['Automatic', 'Manual', 'None']);
            $table->enum('drive_train', ['FWD', 'RWD', 'AWD', '4WD']);

            $table->foreignId('fleet_id')->constrained();
            $table->foreignId('customer_id')->constrained()->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->string('phone');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('address_line1');
            $table->string('address_line2')->nullable();
            $table->string('email')->nullable();
            $table->string('city');
            $table->string('company')->nullable();
            $table->boolean('discount_eligible');
            $table->string('zip_code')->nullable();
            $table->boolean('tax_exempt')->default(0);
            $table->timestamps();

         //   $table->unsignedBigInteger('company_id')
         //   $table->foreign('company_id')->references('id')->on('companies');
            $table->foreignId('country_id')->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}

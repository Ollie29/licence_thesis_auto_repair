<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Customer;
use Faker\Generator as Faker;

$factory->define(Customer::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'phone' => $faker->phoneNumber,
        'address_line1' => $faker->address,
        'address_line2' => $faker->address,
        'email' => $faker->email,
        'company' => $faker->companySuffix,
        'city' => $faker->city,
        'discount_eligible'=>0,
        'zip_code' => $faker->postcode,
        'tax_exempt'=> 0
    ];
});
